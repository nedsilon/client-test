/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      guest: true
    },
    {
      path: '/detail/:article',
      name: 'detail',
      component: () => import('./views/Detail.vue'),
      props: true,
      guest: true
    },
    {
      path: '/:email',
      name: 'profile',
      component: () => import ('./views/Profile.vue'),
      props: true,
      meta: {requiresAuth: true}
    },
  ],
});


router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if(localStorage.getItem('jwt') === null){
      next({
        path: '/',
        params: {nextUrl: to.fullPath}
      })
    }else{
      let user = JSON.parse(localStorage.getItem('user'))
      next()
    }
  }else if(to.matched.some(record => record.meta.guest)) {
    if(localStorage.getItem('jwt') == null){
      next()
    }
    else{
        next({ name: 'home'})
    }
  }else {
    next() 
  }
});

export default router;
