import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';

import './registerServiceWorker';
import Loader from './components/Loader';

/* Third party packages */
import BootstrapVue from 'bootstrap-vue';
import VeeValidate from 'vee-validate';
import Slick from 'vue-slick';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// Boostrap
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Font awesome
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

Vue.use(BootstrapVue);
library.add(faChevronRight, faChevronLeft, faPlus, faMinus, faSpinner);
Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.component('loading', Loader);
Vue.component('slick', Slick);
Vue.use(VeeValidate);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
