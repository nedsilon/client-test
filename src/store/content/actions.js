import Axios from 'axios';

const url = 'http://localhost:5000/api/mainData/';

const actions = {
  fetchData({ commit }) {
    return new Promise((resolve, reject) => {
      commit('helpers/SET_LOADING', true, { root: true });
      Axios.get(url).then((res) => {
        commit('FETCH_DATA', res.data[0]);
        commit('helpers/SET_LOADING', false, { root: true });
        resolve();
      }).catch((err) => {
        console.log(err);
        commit('helpers/SET_LOADING', false, { root: true });
      });
    });
  },
};

export default actions;
