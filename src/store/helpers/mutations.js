/* eslint-disable */
const mutations = {
  SET_LOADING(state, payload) {
    return state.loading = payload;
  },
  SET_AUTH_OVERLAY(state, payload) {
    return state.authOverlay = payload
  }
};

export default mutations;
