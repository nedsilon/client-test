const getters = {
  loading: state => state.loading,
  overlay: state => state.authOverlay,
};

export default getters;
